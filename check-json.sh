#!/bin/bash

echo "Validating style sheet json for correctness..."

for file in styles/*/*.json;
do
    echo $file
    python -mjson.tool $file
done
